# ArtBook
ArtBook est un site internet présentant différentes productions artistiques.

## Environnement
* Symfony 5.3.9
* Symfony CLI v4.26.4
* VueJs 2.6.14
* Docker 20.10.8
* Docker-compose 1.26.2

### Pré-requis
* PHP 7.4
* Composer
* Symfony CLI
* Docker
* Docker-compose
* Yarn (optionnel mais conseillé)
* Npm

Possibilité de vérifier le pré-requis (sauf Docker & Docker-compose) avec la commande suivante (de la CLI Symfony) :

```bash
symfony check:requirements
```

### Branches
* branche principale : master

## Lancer l'environnement de développement
```bash
composer install
 && 
npm install
 && 
npm run build
 && 
docker-compose up -d
 && 
symfony serve -d
```

## Lancer le build pour VueJs
Si yarn est installé (re build automatiquement les fichiers concernés si modification)
```bash
yarn watch
```

Sinon, également possible via nmp (à relancer à chaque modification des fichiers concernés)
```bash
npm run build
```

## Lancer les tests unitaires
L'option ```--testdox``` permet un affichage plus lisible dans la console
```bash
php bin/phpunit
```
