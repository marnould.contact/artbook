<?php

namespace App\Tests;

use App\Entity\Category;
use DateTime;
use PHPUnit\Framework\TestCase;

class CategoryUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $category = new Category();
        $now = new DateTime('now');

        $category->setTitle('TestTitle')
                 ->setDescription('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ex.')
                 ->setType('testType')
                 ->setSlug('category-test')
                 ->setCreatedOn($now)
                 ->setUpdatedOn($now);

        $this->assertTrue($category->getTitle() === 'TestTitle');
        $this->assertTrue($category->getDescription() === 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ex.');
        $this->assertTrue($category->getType() === 'testType');
        $this->assertTrue($category->getSlug() === 'category-test');
        $this->assertTrue($category->getCreatedOn() === $now);
        $this->assertTrue($category->getUpdatedOn() === $now);
    }

    public function testIsFalse(): void
    {
        $category = new Category();
        $now = new DateTime('now');

        $category->setTitle('TestTitle')
            ->setDescription('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ex.')
            ->setType('testType')
            ->setSlug('category-test')
            ->setCreatedOn($now)
            ->setUpdatedOn($now);

        $this->assertFalse($category->getTitle() === 'FalseTestTitle');
        $this->assertFalse($category->getDescription() === 'FALSE Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ex.');
        $this->assertFalse($category->getType() === 'falseTestType');
        $this->assertFalse($category->getSlug() === 'category-test-false');
        $this->assertFalse($category->getCreatedOn() === new DateTime('1969-09-02 09:54:22'));
        $this->assertFalse($category->getUpdatedOn() === new DateTime('1969-09-02 09:54:22'));
    }

    public function testIsEmpty(): void
    {
        $category = new Category();

        $this->assertEmpty($category->getTitle());
        $this->assertEmpty($category->getDescription());
        $this->assertEmpty($category->getType());
        $this->assertEmpty($category->getSlug());
        $this->assertEmpty($category->getCreatedOn());
        $this->assertEmpty($category->getUpdatedOn());
    }
}
