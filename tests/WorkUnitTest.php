<?php

namespace App\Tests;

use App\Entity\Category;
use App\Entity\Comment;
use App\Entity\User;
use App\Entity\Work;
use DateTime;
use PHPUnit\Framework\TestCase;

class WorkUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $work = new Work();
        $user = new User();
        $category = new Category();
        $comment = new Comment();
        $now = new DateTime('now');

        $work->setTitle('TestTitle')
            ->setDescription('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ex.')
            ->setType('testType')
            ->setSlug('work-test')
            ->setPortfolio(true)
            ->setFilePath('/home/my_user_name/my_file')
            ->setCompletionDate($now)
            ->setCreatedOn($now)
            ->setUpdatedOn($now)
            ->setUser($user)
            ->addCategory($category)
            ->addComment($comment)
        ;

        $this->assertTrue($work->getTitle() === 'TestTitle');
        $this->assertTrue($work->getDescription() === 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ex.');
        $this->assertTrue($work->getType() === 'testType');
        $this->assertTrue($work->getSlug() === 'work-test');
        $this->assertTrue($work->getPortfolio() === true);
        $this->assertTrue($work->getFilePath() === '/home/my_user_name/my_file');
        $this->assertTrue($work->getCompletionDate() === $now);
        $this->assertTrue($work->getCreatedOn() === $now);
        $this->assertTrue($work->getUpdatedOn() === $now);
        $this->assertContains($category, $work->getCategory());
        $this->assertContains($comment, $work->getComments());
        $this->assertTrue($work->getUser() === $user);
    }

    public function testIsFalse(): void
    {
        $work = new Work();
        $user = new User();
        $category = new Category();
        $comment = new Comment();        $now = new DateTime('now');

        $work->setTitle('TestTitle')
            ->setDescription('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ex.')
            ->setType('testType')
            ->setSlug('work-test')
            ->setPortfolio(true)
            ->setFilePath('/home/my_user_name/my_file')
            ->setCompletionDate($now)
            ->setCreatedOn($now)
            ->setUpdatedOn($now)
            ->setUser($user)
            ->addCategory($category)
            ->addComment($comment)
        ;

        $this->assertFalse($work->getTitle() === 'FalseTestTitle');
        $this->assertFalse($work->getDescription() === 'FALSE Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ex.');
        $this->assertFalse($work->getType() === 'falseTestType');
        $this->assertFalse($work->getSlug() === 'false-work-test');
        $this->assertFalse($work->getPortfolio() === false);
        $this->assertFalse($work->getFilePath() === '/home/my_user_name/my_false_file');
        $this->assertFalse($work->getCompletionDate() === new DateTime('1969-09-02 09:54:22'));
        $this->assertFalse($work->getCreatedOn() === new DateTime('1969-09-02 09:54:22'));
        $this->assertFalse($work->getUpdatedOn() === new DateTime('1969-09-02 09:54:22'));
        $this->assertNotContains(new Category(), $work->getCategory());
        $this->assertNotContains(new Comment(), $work->getComments());
        $this->assertFalse($work->getUser() === new User);
    }

    public function testIsEmpty(): void
    {
        $work = new Work();

        $this->assertEmpty($work->getTitle());
        $this->assertEmpty($work->getDescription());
        $this->assertEmpty($work->getType());
        $this->assertEmpty($work->getSlug());
        $this->assertEmpty($work->getPortfolio());
        $this->assertEmpty($work->getFilePath());
        $this->assertEmpty($work->getCompletionDate());
        $this->assertEmpty($work->getCreatedOn());
        $this->assertEmpty($work->getUpdatedOn());
        $this->assertEmpty($work->getCategory());
        $this->assertEmpty($work->getUser());
        $this->assertEmpty($work->getComments());
    }
}
