<?php

namespace App\Tests;

use App\Entity\User;
use DateTime;
use PHPUnit\Framework\TestCase;

class UserUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $user = new User();
        $now = new DateTime('now');

        $user->setEmail('test@test.com')
             ->setFirstName('TestFirstName')
             ->setLastName('TestLastName')
             ->setPassword('testPassword')
             ->setPhoneNumber('0102030405')
             ->setAbout('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ex.')
             ->setSocialMedias([
                'facebook' => 'https://fr-fr.facebook.com/',
                'instagram' => 'https://www.instagram.com/?hl=fr',
                'linkedin' => 'https://www.linkedin.com/'
             ])
             ->setCreatedOn($now)
             ->setUpdatedOn($now);

        $this->assertTrue($user->getEmail() === 'test@test.com');
        $this->assertTrue($user->getFirstName() === 'TestFirstName');
        $this->assertTrue($user->getLastName() === 'TestLastName');
        $this->assertTrue($user->getPassword() === 'testPassword');
        $this->assertTrue($user->getPhoneNumber() === '0102030405');
        $this->assertTrue($user->getAbout() === 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ex.');
        $this->assertTrue($user->getSocialMedias() === ['facebook' => 'https://fr-fr.facebook.com/', 'instagram' => 'https://www.instagram.com/?hl=fr', 'linkedin' => 'https://www.linkedin.com/']);
        $this->assertTrue($user->getCreatedOn() === $now);
        $this->assertTrue($user->getUpdatedOn() === $now);
    }

    public function testIsFalse(): void
    {
        $user = new User();

        $user->setEmail('test@test.com')
            ->setFirstName('TestFirstName')
            ->setLastName('TestLastName')
            ->setPassword('testPassword')
            ->setPhoneNumber('0102030405')
            ->setAbout('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ex.')
            ->setSocialMedias([
                'facebook' => 'https://fr-fr.facebook.com/',
                'instagram' => 'https://www.instagram.com/?hl=fr',
                'linkedin' => 'https://www.linkedin.com/'
            ])
            ->setCreatedOn(new DateTime('now'))
            ->setUpdatedOn(new DateTime('now'));

        $this->assertFalse($user->getEmail() === 'false@test.com');
        $this->assertFalse($user->getFirstName() === 'TestFalseFirstName');
        $this->assertFalse($user->getLastName() === 'TestFalseLastName');
        $this->assertFalse($user->getPassword() === 'testFalsePassword');
        $this->assertFalse($user->getPhoneNumber() === '0504030201');
        $this->assertFalse($user->getAbout() === 'FALSE Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ex.');
        $this->assertFalse($user->getSocialMedias() === ['facebook' => 'https://fr-fr.false.com/', 'instagram' => 'https://www.false.com/?hl=fr', 'linkedin' => 'https://www.false.com/']);
        $this->assertFalse($user->getCreatedOn() === new DateTime('1969-09-02 09:54:22'));
        $this->assertFalse($user->getUpdatedOn() === new DateTime('1969-09-02 09:54:22'));
    }

    public function testIsEmpty(): void
    {
        $user = new User();

        $this->assertEmpty($user->getEmail());
        $this->assertEmpty($user->getFirstName());
        $this->assertEmpty($user->getLastName());
        $this->assertEmpty($user->getPhoneNumber());
        $this->assertEmpty($user->getAbout());
        $this->assertEmpty($user->getSocialMedias());
        $this->assertEmpty($user->getCreatedOn());
        $this->assertEmpty($user->getUpdatedOn());
    }
}
