<?php

namespace App\Tests;

use App\Entity\Comment;
use DateTime;
use PHPUnit\Framework\TestCase;

class CommentUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $comment = new Comment();
        $now = new DateTime('now');

        $comment->setEmail('test@test.com')
            ->setContent('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ex.')
            ->setAuthor('testAuthor')
            ->setCreatedOn($now)
            ->setUpdatedOn($now);

        $this->assertTrue($comment->getEmail() === 'test@test.com');
        $this->assertTrue($comment->getContent() === 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ex.');
        $this->assertTrue($comment->getAuthor() === 'testAuthor');
        $this->assertTrue($comment->getCreatedOn() === $now);
        $this->assertTrue($comment->getUpdatedOn() === $now);
    }

    public function testIsFalse(): void
    {
        $comment = new Comment();
        $now = new DateTime('now');

        $comment->setEmail('test@test.com')
            ->setContent('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ex.')
            ->setAuthor('testAuthor')
            ->setCreatedOn($now)
            ->setUpdatedOn($now);

        $this->assertFalse($comment->getEmail() === 'false@test.com');
        $this->assertFalse($comment->getContent() === 'FALSE Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ex.');
        $this->assertFalse($comment->getAuthor() === 'falseTestAuthor');
        $this->assertFalse($comment->getCreatedOn() === new DateTime('1969-09-02 09:54:22'));
        $this->assertFalse($comment->getUpdatedOn() === new DateTime('1969-09-02 09:54:22'));
    }

    public function testIsEmpty(): void
    {
        $comment = new Comment();

        $this->assertEmpty($comment->getEmail());
        $this->assertEmpty($comment->getContent());
        $this->assertEmpty($comment->getAuthor());
        $this->assertEmpty($comment->getCreatedOn());
        $this->assertEmpty($comment->getUpdatedOn());
    }
}
