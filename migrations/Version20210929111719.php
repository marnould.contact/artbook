<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210929111719 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Create relations with User, Category and Comment entities for Work entity';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE work_category (work_id INT NOT NULL, category_id INT NOT NULL, INDEX IDX_D3F9939DBB3453DB (work_id), INDEX IDX_D3F9939D12469DE2 (category_id), PRIMARY KEY(work_id, category_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE work_category ADD CONSTRAINT FK_D3F9939DBB3453DB FOREIGN KEY (work_id) REFERENCES work (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE work_category ADD CONSTRAINT FK_D3F9939D12469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE comment ADD work_id INT NOT NULL');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526CBB3453DB FOREIGN KEY (work_id) REFERENCES work (id)');
        $this->addSql('CREATE INDEX IDX_9474526CBB3453DB ON comment (work_id)');
        $this->addSql('ALTER TABLE work ADD user_id INT NOT NULL');
        $this->addSql('ALTER TABLE work ADD CONSTRAINT FK_534E6880A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_534E6880A76ED395 ON work (user_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE work_category');
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526CBB3453DB');
        $this->addSql('DROP INDEX IDX_9474526CBB3453DB ON comment');
        $this->addSql('ALTER TABLE comment DROP work_id');
        $this->addSql('ALTER TABLE work DROP FOREIGN KEY FK_534E6880A76ED395');
        $this->addSql('DROP INDEX IDX_534E6880A76ED395 ON work');
        $this->addSql('ALTER TABLE work DROP user_id');
    }
}
